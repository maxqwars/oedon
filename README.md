# oedon
Console utility for analyzing comments under VKontakte social network posts

## Installation

Oedon is available for installation from the npm

> $ npm install -g @tns/oedon

## Before use Oedon

Before using Oedon you need to create a text file containing the access token for requests to the VKontakte API.

> $ touch token.key && echo <you_access_token> >> token.key

## Commands reference

### find-comments

### find-keywords

### get-id

### extract