#!/usr/bin/env node

// External modules
import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import { argv } from "node:process";

// Internal modules
import { OedonController } from "./OedonController";

// Init Oedon Instance
const oedon = new OedonController();

yargs(hideBin(argv))
  .scriptName("Oedon")
  .usage("$0 <cmd> [args]")
  .command(
    "find-comments <from_id> <targets> <depth>",
    "Find user comments",
    (yargs) =>
      yargs
        .positional("from_id", { type: "number" })
        .positional("targets", { type: "string" })
        .positional("depth", { type: "number", default: -1 }),
    (argv) => {
      const { from_id, targets, depth } = argv;
      oedon.findComments(from_id, targets, depth);
    }
  )
  .command(
    "get-id <url>",
    "Get user/group id",
    (yargs) => yargs.positional("url", { type: "string" }),
    (argv) => {
      const { url } = argv;
      oedon.getId(url);
    }
  )
  .command(
    "extract <url>",
    "Export user groups & friends",
    (yargs) => yargs.positional("url", { type: "string" }),
    (argv) => {
      const { url } = argv;
      oedon.extract(url);
    }
  )
  .command(
    "find-keywords <exp> <targets> <depth>",
    "Find user comments contain keywords",
    (yargs) =>
      yargs
        .positional("exp", { type: "string" })
        .positional("targets", { type: "string" })
        .positional("depth", { type: "number", default: -1 }),
    (argv) => {
      const { exp, targets, depth } = argv;
      oedon.findKeywords(exp, targets, depth);
    }
  )
  .help().argv;
