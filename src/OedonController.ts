export class OedonController {
  private readonly _model = null;

  public async findComments(
    fromId: number,
    targetsFilename: string,
    depth: number
  ) {
    throw new Error("Not implemented!");
  }

  public async getId(url: string) {
    throw new Error("Not implemented!");
  }

  public async extract(url: string) {
    throw new Error("Not implemented!");
  }

  public async findKeywords(
    expression: string,
    targetsFilename: string,
    depth?: number
  ) {
    throw new Error("Not implemented!");
  }
}
