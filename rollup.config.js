import typescriptPlugin from "rollup-plugin-typescript2";
import shebang from "rollup-plugin-preserve-shebang";

const dist = "dist";

export default {
  input: "./src/index.ts",
  plugins: [
    typescriptPlugin(),
    shebang({
      shebang: "#!/usr/bin/env node",
    }),
  ],
  output: [
    {
      file: `${dist}/index.cjs`,
      format: "cjs",
    },
    {
      file: `${dist}/index.mjs`,
      format: "esm",
    },
  ],
  external: [
    "yargs",
    "yargs/helpers",
    "node:process",
    "node:fs/promises",
    "node:path",
    "colorette",
    "vk-io"
  ],
};
